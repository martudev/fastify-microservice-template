/* eslint-disable no-empty */
module.exports = fastify => {
  const service = require('./service')(fastify);

  //eslint-disable-next-line
  const { config } = fastify;

  const method = async (request, reply) => {
    const { queryOne, queryTwo } = request.query;

    reply.code(200).send({ queryOne, queryTwo });
  };

  const methodCT = async (request, reply) => {
    const { query } = request;

    const products = await service.getProducts(query);

    reply.code(200).send(products);
  };

  const addDiscountCodeToCart = async (request, reply) => {
    const { cartId } = request.params;
    const { code } = request.body;

    const cart = await service.updateCartDiscount(cartId, code);

    reply.code(200).send(cart);
  };

  const createCustomer = async (request, reply) => {
    const body = request.body;

    const customer = await service.createCustomer(body);

    reply.code(200).send(customer);
  };

  const confirmPayment = async (request, reply) => {
    const { custom, amountPlanned } = request.body.resource.obj;
    const { customerId } = custom.fields;

    const data = {
      customerId,
      amount: amountPlanned.centAmount,
      currency: amountPlanned.currencyCode
    };

    console.log(data);

    const response = await service.confirmPayment(data);

    console.log(response);

    reply.header('Access-Control-Allow-Origin', '*');
    reply.code(200).send({ actions: [] });
  };

  const createPayment = async (request, reply) => {
    const { customerId } = request.body;

    const response = await service.createPayment(customerId);

    reply.header('Access-Control-Allow-Origin', '*');
    reply.send({ client_secret: response.client_secret });
  };

  const createStripeCustomer = async (request, reply) => {
    const customer = await service.createStripeCustomer();

    reply.header('Access-Control-Allow-Origin', '*');
    reply.send({ id: customer.id });
  };

  const createApiExtension = async (request, reply) => {
    const response = await service.createApiExtension(request.body);

    reply.send(response);
  };

  const createType = async (request, reply) => {
    const response = await service.createType(request.body);

    reply.send(response);
  };

  return {
    method,
    methodCT,
    addDiscountCodeToCart,
    createCustomer,
    confirmPayment,
    createPayment,
    createApiExtension,
    createType,
    createStripeCustomer
  };
};
