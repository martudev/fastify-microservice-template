module.exports = fastify => {
  const { ProductProjectionRepository } = fastify.commercetools.repositories;

  const { client, requestBuilder } = fastify.commercetools;

  const stripe = require('stripe')(
    'sk_test_51KIz8vFWsQK9wZRJc5lz8hwzqkS1KcmP42Rla1Q09fhzg4fCVdHIncCQQ9ZvpOKsOKf9j8hSGWvjEwF9rag29AW3008PphsbXB'
  );

  const service = {};

  service.getProducts = async query => {
    return ProductProjectionRepository.find(query);
  };

  service.getCart = async cartId => {
    return await client.execute({
      uri: requestBuilder().carts.byId(cartId).build(),
      method: 'GET'
    });
  };

  service.updateCartDiscount = async (cartId, code) => {
    const cart = await service.getCart(cartId);
    return await client.execute({
      uri: requestBuilder().carts.byId(cartId).build(),
      method: 'POST',
      body: {
        version: cart.body.version,
        actions: [
          {
            action: 'addDiscountCode',
            code: code
          }
        ]
      }
    });
  };

  service.createCustomer = async body => {
    return await client.execute({
      uri: requestBuilder().customers.build(),
      method: 'POST',
      body: body
    });
  };

  service.createPayment = async customerId => {
    return await stripe.setupIntents.create({
      customer: customerId,
      payment_method_types: ['card']
    });
  };

  service.confirmPayment = async ({ amount, currency, customerId }) => {
    try {
      const paymentMethods = await stripe.paymentMethods.list({
        customer: customerId,
        type: 'card'
      });
      return await stripe.paymentIntents.create({
        amount: amount,
        currency: currency,
        customer: customerId,
        payment_method: paymentMethods.data[0].id,
        off_session: true,
        confirm: true
      });
    } catch (err) {
      // Error code will be authentication_required if authentication is needed
      console.log('Error code is: ', err.code);
      const paymentIntentRetrieved = await stripe.paymentIntents.retrieve(
        err.raw.payment_intent.id
      );
      console.log('PI retrieved: ', paymentIntentRetrieved.id);
    }
  };

  service.createStripeCustomer = async () => {
    return await stripe.customers.create();
  };

  service.createApiExtension = async body => {
    return await client.execute({
      uri: requestBuilder().extensions.build(),
      method: 'POST',
      body: body
    });
  };

  service.createType = async body => {
    return await client.execute({
      uri: requestBuilder().types.build(),
      method: 'POST',
      body: body
    });
  };

  return service;
};
